package de.tentact.coinsapi;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Source {

    static File mysqlfile = new File("plugins/Coinsapi","mysql.yml");
    static FileConfiguration cfg = YamlConfiguration.loadConfiguration(mysqlfile);

    public static void createMysqlCfg() {
        cfg.addDefault("mysql.host", "127.0.0.1");
        cfg.addDefault("mysql.username", "coinsapi");
        cfg.addDefault("mysql.database", "coinsapi");
        cfg.addDefault("mysql.password", "password");
        cfg.addDefault("mysql.port", 3306);

        cfg.options().copyDefaults(true);

        try {
            cfg.save(mysqlfile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        CoinsAPI.getInstance().getMySQLInstance().host = cfg.getString("mysql.host");
        CoinsAPI.getInstance().getMySQLInstance().username = cfg.getString("mysql.username");
        CoinsAPI.getInstance().getMySQLInstance().database = cfg.getString("mysql.database");
        CoinsAPI.getInstance().getMySQLInstance().password = cfg.getString("mysql.password");
        CoinsAPI.getInstance().getMySQLInstance().port = cfg.getInt("mysql.port");

    }

}