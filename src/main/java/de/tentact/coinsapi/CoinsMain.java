package de.tentact.coinsapi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class CoinsMain extends JavaPlugin {

    static Connection con;


    @Override
    public void onEnable() {

        Source.createMysqlCfg();
        mysqlSetup();
        CoinsAPI.getInstance().getMySQLInstance().update();

    }

    public void mysqlSetup() {

        try {

            synchronized (this) {

                if (con != null && con.isClosed()) {
                    return;
                }

                Class.forName("com.mysql.jdbc.Driver");
                setConnection(DriverManager.getConnection(
                        "jdbc:mysql://" + CoinsAPI.getInstance().getMySQLInstance().host+ ":" + CoinsAPI.getInstance().getMySQLInstance().port + "/" + CoinsAPI.getInstance().getMySQLInstance().database, CoinsAPI.getInstance().getMySQLInstance().username,
                        CoinsAPI.getInstance().getMySQLInstance().password));

                Bukkit.getConsoleSender().sendMessage("§3Mysql connected!");

            }

        } catch (SQLException e) {
            e.printStackTrace();

        } catch (ClassNotFoundException e) {

        }
        //A
    }
    public void setConnection(Connection connection) {
        con = connection;
    }

    public void close() {
        try {
            if (con != null) {
                con.close();
                System.out.println("[MySQL] Die Verbindung zur MySQL wurde Erfolgreich beendet!");
            }
        } catch (SQLException e) {
            System.out.println("[MySQL] Fehler beim beenden der Verbindung zur MySQL! Fehler: " + e.getMessage());
        }
    }



}