package de.tentact.coinsapi;

import org.bukkit.entity.Player;

public class CoinsAPI {
    public static CoinsAPI instance;
    public static CoinsMySQL mysqlinstance;


    private CoinsAPI() {

    }

    public static CoinsAPI getInstance() {
        if(CoinsAPI.instance == null) {
            CoinsAPI.instance = new CoinsAPI();
        }
        return CoinsAPI.instance;
    }
    public void addCoins(Player player,int Coins) {
        CoinsAPI.getInstance().getMySQLInstance().createPlayer(player.getUniqueId());
        CoinsAPI.getInstance().getMySQLInstance().add_Coins(player.getUniqueId(), Coins);
        int i;
    }

    public void resetCoins(Player player) {
        CoinsAPI.getInstance().getMySQLInstance().createPlayer(player.getUniqueId());
        CoinsAPI.getInstance().getMySQLInstance().set_Coins(player.getUniqueId(), 0);

    }

    public int seeCoins(Player player) {
        CoinsAPI.getInstance().getMySQLInstance().createPlayer(player.getUniqueId());
        return CoinsAPI.getInstance().getMySQLInstance().get_Coins(player.getUniqueId());

    }

    public void removeCoins(Player player,int Coins) {
        CoinsAPI.getInstance().getMySQLInstance().createPlayer(player.getUniqueId());
        if(seeCoins(player) - Coins >= 0) {
            CoinsAPI.getInstance().getMySQLInstance().add_Coins(player.getUniqueId(), -Coins);
        }

    }
    public CoinsMySQL getMySQLInstance() {
        if(mysqlinstance == null) {
            mysqlinstance = new CoinsMySQL();
        }
        return mysqlinstance;
    }






}