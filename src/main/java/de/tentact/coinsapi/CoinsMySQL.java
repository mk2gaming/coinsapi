package de.tentact.coinsapi;

import org.bukkit.plugin.Plugin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;


public class CoinsMySQL {

    public String host, username, password, database;
    public int port, COINS;

    public CoinsMySQL() {

    }

    private boolean playerExists(UUID uuid) {

        try {
            PreparedStatement ps = CoinsMain.con.prepareStatement("SELECT * FROM coinsapi WHERE UUID=?");
            ps.setString(1, uuid.toString());

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return false;

    }

    public void createPlayer(final UUID uuid) {

        try {
            PreparedStatement ps = CoinsMain.con.prepareStatement("SELECT * FROM coinsapi WHERE UUID=?");

            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();
            rs.next();
            if (playerExists(uuid) != true) {
                PreparedStatement insert = CoinsMain.con
                        .prepareStatement("INSERT INTO coinsapi (UUID, Coins) VALUE (?,?)");
                insert.setString(1, uuid.toString());
                insert.setInt(2, 0);
                insert.executeUpdate();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void update() {
        try {
            PreparedStatement ps = CoinsMain.con
                    .prepareStatement("CREATE TABLE IF NOT EXISTS coinsapi(UUID varchar(64),Coins int);");
            ps.executeUpdate("CREATE TABLE IF NOT EXISTS coinsapi(UUID varchar(64), Coins int);");

        } catch (SQLException e) {

            System.err.println(e);
        }
    }
    public int get_Coins(UUID uuid) {

        try {
            PreparedStatement ps = CoinsMain.con.prepareStatement("SELECT * FROM coinsapi WHERE UUID=?");
            ps.setString(1, uuid.toString());

            ResultSet rs = ps.executeQuery();
            rs.next();
            COINS = rs.getInt("Coins");
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return COINS;

    }
    public void add_Coins(UUID uuid, int coins) {

        try {
            int addedcoins = CoinsAPI.getInstance().getMySQLInstance().get_Coins(uuid)+coins;
            PreparedStatement ps =CoinsMain.con.prepareStatement("UPDATE coinsapi SET Coins=? WHERE UUID=?");
            ps.setInt(1, addedcoins);
            ps.setString(2, uuid.toString());
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void set_Coins(UUID uuid, int coins) {
        try {
            PreparedStatement ps = CoinsMain.con.prepareStatement("UPDATE coinsapi SET Coins=? WHERE UUID=?");
            ps.setInt(1, coins);
            ps.setString(2, uuid.toString());
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}